﻿Shader "Custom/Soph_magicRampScalingOutline" {
        Properties {
                _Color ("Color", Color) = (1,1,1,1)
                _OutlineColor ("Outline Color", Color) = (0,0,0,1)
                _Outline ("Outline width", Range (.002, 0.05)) = .005
                _OutlineScaler ("Outline Scaler", Range (.001, 1.0)) = .5
                _Ramp ("ramp light", 2D) = "gray" {}
        }
       
CGINCLUDE
#include "UnityCG.cginc"
 
struct appdata {
        float4 vertex : POSITION;
        float3 normal : NORMAL;
};
 
struct v2f {
        float4 pos : POSITION;
        float4 color : COLOR;
};
 
uniform float _Outline;
uniform float _OutlineScaler;
uniform float4 _OutlineColor;
 
v2f vert(appdata v) {
        // just make a copy of incoming vertex data but scaled according to normal direction
        v2f o;
        o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
 
        float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
        float2 offset = TransformViewToProjection(norm.xy);
 
        o.pos.xy += offset * o.pos.z * _OutlineScaler * _Outline;
        o.color = _OutlineColor;
        return o;
}
ENDCG
       
        SubShader {
                Tags { "RenderType" = "Opaque" }
                CGPROGRAM
                #pragma surface surf ToonRamp
                //#pragma target 2.0
 
                sampler2D _Ramp;
               
               
                struct Input {
                        float2 uv_Ramp;
                };
               
                half4 LightingToonRamp (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
                       
                        half diff = max (0, dot ( lightDir, s.Normal ));
                       
                        half4 res;
                        res.rgb = _LightColor0.rgb * diff;
                        res *= atten * 2.0;
                       
                        //res.x = 0.0;
                        //res.y = 0.0;
                       
                        res.y = s.Alpha.r;
                       
                        //s.Emission = 0.0;
                       
                        float4 finalThing = tex2D(_Ramp,res.xy);
                        finalThing.rgb *= s.Albedo.rgb;
                       
                        return finalThing;
                }
               
               
               
               
                float4 _Color;
                void surf (Input IN, inout SurfaceOutput o) {
                       
                        o.Alpha = 1.0;
                        //o.Albedo = 0.0;
                        o.Emission = 0.0;
                        o.Gloss = 0.0;
                        o.Specular = 0.0;
                       
                        //float2 theUV = IN.uv_Ramp;
                        //theUV.x=0.5;
                       
                        o.Alpha.r = IN.uv_Ramp.y;
                       
                        o.Albedo = _Color.rgb;// * tex2D(_Ramp, theUV);
                }
        ENDCG
       
        Pass {
                        Name "OUTLINE"
                        Tags { "LightMode" = "Always" }
                        Cull Front
                        ZWrite On
                        ColorMask RGB
                        Blend SrcAlpha OneMinusSrcAlpha
                        //Offset 50,50
 
                        CGPROGRAM
                        #pragma vertex vert
                        #pragma fragment frag
                        half4 frag(v2f i) :COLOR { return i.color; }
                        ENDCG
                }
       
        }
FallBack "Diffuse"
}