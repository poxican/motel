﻿using UnityEngine;
using System.Collections;

public class RendererEnabledOnStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
		if (gameObject.renderer.enabled == false)
		{
			gameObject.renderer.enabled = true;
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
