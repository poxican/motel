﻿using UnityEngine;
using System.Collections;

public class LightSwitch : MonoBehaviour {
	
	private bool switchOn;
	public GameObject pointLight;
	public GameObject lightSwitch;
	public float switchOnDegrees;
	public float switchOffDegress;
	public float switchTime;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void SwitchOnOff()
	{
		Debug.Log("switchOnOff");
		
		if (switchOn)
		{
			pointLight.light.enabled = false;
			iTween.RotateTo(lightSwitch, iTween.Hash ("x",-30, "time",switchTime));
			switchOn = !switchOn;
		}
		else
		{
			pointLight.light.enabled = true;
			iTween.RotateTo(lightSwitch, iTween.Hash ("x",30, "time",switchTime));
			switchOn = !switchOn;

		}
		
	}
	
}
