﻿using UnityEngine;
using System.Collections;

public class Doors : MonoBehaviour {
	
	public float doorTargetVelocity;
	public float doorHingeJointForce;
	public float trapdoorForceUp;
	public float trapdoorForceDown;
	
	public bool door;
	public bool lid;
	public bool window;
	public bool open = false;
	
	public Vector3 lidDownDirection;

	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	
	}
	
	public void useDoor()
	{
		if (door)
		{
			Debug.Log (gameObject.transform.name + " used");
		
			HingeJoint doorHingeJoint = gameObject.GetComponent<HingeJoint>();
		
			if (doorHingeJoint.motor.targetVelocity == -doorTargetVelocity)
			{
				Debug.Log ("stoveDoor close used, doorHingeJoint.motor.targetVelocity = "+ doorHingeJoint.motor.targetVelocity);
				JointMotor jointMotor = doorHingeJoint.motor;
				jointMotor.targetVelocity = doorTargetVelocity;
				//jointMotor.force = 100;
				doorHingeJoint.motor = jointMotor;
			}
		
			else if (doorHingeJoint.motor.targetVelocity == doorTargetVelocity)
			{
				Debug.Log ("stoveDoor open used, doorHingeJoint.motor.targetVelocity = "+ doorHingeJoint.motor.targetVelocity);
				JointMotor jointMotor = doorHingeJoint.motor;
				jointMotor.targetVelocity = -doorTargetVelocity;
				doorHingeJoint.motor = jointMotor;
			}
		}
		if (lid)
		{
			HingeJoint doorHingeJoint = gameObject.GetComponent<HingeJoint>();

			if (open == true)
			{
				doorHingeJoint.useMotor = false;
				transform.rigidbody.AddForce(lidDownDirection * trapdoorForceDown);
				open = false;
				Debug.Log ("close " + transform.name);
			}
			else if (open == false)
			{
				//transform.rigidbody.AddForce(Vector3.up * trapdoorForceUp);
				doorHingeJoint.useMotor = true;
				open = true;
				Debug.Log ("open = " + transform.name);

			}
		}	
		
		if (window)
		{
			
		}
			
	}
	
}
