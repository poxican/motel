﻿using UnityEngine;
using System.Collections;

public class Interaction : MonoBehaviour {
	
	public float interactionDistance;
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	//Input Control
		if (Input.GetButtonDown("UseLeft"))
		{
			Use();
		}
		
		if (Input.GetButtonDown("UseRight"))
		{
			Use();
		}
		
	}
	
	
	void Use()
	{
		Debug.Log("Use");

		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		RaycastHit useHit;
		
		//Raycasting
		if(Physics.Raycast(transform.position, fwd, out useHit, interactionDistance))
		{
			if (useHit.transform.name == "Drawer")
			{
				GameObject drawer = useHit.transform.gameObject;
				
				Debug.Log ("Open Drawer");
				
				Drawers drawersScript = drawer.GetComponent<Drawers>();
				
				drawersScript.OpenCloseDrawer();
			}
			
			if (useHit.transform.name == "Curtain")
			{
				GameObject curtain = useHit.transform.gameObject;
				
				Debug.Log ("Open Curtain");
				
				Curtains curtainsScript = curtain.GetComponent<Curtains>();
				
				curtainsScript.OpenCloseCurtain();
			}
			
			if (useHit.transform.name == "Switch")
			{
				GameObject lightSwitch = useHit.transform.gameObject;
				
				Debug.Log ("useHit = Switch");
				
				LightSwitch switchScript = lightSwitch.GetComponent<LightSwitch>();
				
				switchScript.SwitchOnOff();
			}
			
			if (useHit.transform.name == "Tap")
			{
				GameObject tap = useHit.transform.gameObject;
				
				Debug.Log ("useHit = Tap");
				
				Tap tapScript = tap.GetComponent<Tap>();
				
				tapScript.TapOnOff();
			}

			if (useHit.transform.name == "Door")
			{
				GameObject door = useHit.transform.gameObject;
				
				Debug.Log ("useHit = Door");
				
				Doors doorScript = door.GetComponent<Doors>();
				
				doorScript.useDoor();
			}
			
		}
	}
}
