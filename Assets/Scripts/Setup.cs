﻿using UnityEngine;
using System.Collections;

public class Setup : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		Screen.lockCursor = true;
		Screen.showCursor = false;
		RenderSettings.fog = true;

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
