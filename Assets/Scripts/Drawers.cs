﻿using UnityEngine;
using System.Collections;

public class Drawers : MonoBehaviour {
	
	public float openPosition;
	public float closedPosition;
	public float openCloseTime;
	public string easeType;
	
	private bool isOpen;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OpenCloseDrawer()
	{
		if (isOpen)
		{
			Debug.Log ("OpenCloseDrawer - Close");
			iTween.MoveTo(gameObject, iTween.Hash ("z", closedPosition, "time",openCloseTime, "isLocal", true, "easeType",easeType));
			isOpen = !isOpen;
		}
		else
		{
			Debug.Log ("OpenCloseDrawer - Open");
			iTween.MoveTo(gameObject, iTween.Hash ("z", openPosition, "time",openCloseTime, "isLocal", true, "easeType",easeType));
			isOpen = !isOpen;
		}
	}
}
