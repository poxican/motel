﻿using UnityEngine;
using System.Collections;

public class Tap : MonoBehaviour {
	
	private bool tapOn = false;
	public float tapOnDegrees;
	public float tapOffDegrees;
	public float turnTime;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void TapOnOff()
	{
		Debug.Log("switchOnOff");
		
		if (tapOn)
		{
			iTween.RotateBy(transform.gameObject, iTween.Hash ("z",0.5, "time",turnTime));
			tapOn = !tapOn;
			Debug.Log ("Turn off");
		}
		else
		{
			iTween.RotateBy(transform.gameObject, iTween.Hash ("z",-0.5, "time",turnTime));
			tapOn = !tapOn;
			Debug.Log ("Turn on");

		}
		
	}
	
}
