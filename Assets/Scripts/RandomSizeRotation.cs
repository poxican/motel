﻿using UnityEngine;
using System.Collections;

public class RandomSizeRotation : MonoBehaviour {

	private Vector3 randomScale;
	private Vector3 randomRotation;
	private float sizeMultiplier;
	public float sizeMultiplierMax;

	// Use this for initialization
	void Start () {



		sizeMultiplier = Random.Range(0.5f, sizeMultiplierMax);

		randomScale.x = transform.localScale.x * sizeMultiplier;
		randomScale.y = transform.localScale.y * sizeMultiplier;
		randomScale.z = transform.localScale.z * sizeMultiplier;

		//randomRotation.y = Random.Range (0f, 360f);

		randomRotation = new Vector3(transform.eulerAngles.x,(Random.Range (0,360)),0);

		transform.localScale = randomScale;

		//transform.localRotation = Vector3(0f,randomRotation,0f);
		transform.eulerAngles = randomRotation;

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
