﻿using UnityEngine;
using System.Collections;

public class Curtains : MonoBehaviour {
	
	public float openScaleX;
	public float openScaleY;
	public float openScaleZ;
	public float closedScaleX;
	public float closedScaleY;
	public float closedScaleZ;
	public float openCloseTime;
	public bool isOpen;
	public string easeType;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OpenCloseCurtain()
	{
		if (isOpen)
		{
			Debug.Log ("OpenCloseCurtain - Close");
			iTween.ScaleTo(gameObject, iTween.Hash ("x", closedScaleX, "y", closedScaleY, "z", closedScaleZ, "time",openCloseTime, "isLocal", true, "easeType",easeType));
			isOpen = !isOpen;
		}
		else
		{
			Debug.Log ("OpenCloseCurtain - Open");
			iTween.ScaleTo(gameObject, iTween.Hash ("x", openScaleX, "y", openScaleY, "z", openScaleZ, "time",openCloseTime, "isLocal", true, "easeType",easeType));
			isOpen = !isOpen;
		}
	}
}
